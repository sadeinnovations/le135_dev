import json
import boto3
import os
import logging
import datetime


logger = logging.getLogger()
logger.setLevel(logging.INFO)

def mqttSend(event, context):

    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table(os.environ['CONTROL_MESSAGES_TABLE'])

    logger.info(json.dumps(event))

    # Incoming message format
    #	{
	#	  "controlId": "b9eaf0fa-a4c9-5553-9f7d-b02164979338"
    #   }
    get_uuid = event["controlId"]
    print(f'controlId: {get_uuid}')

    # get time
    tsi =(int)(datetime.datetime.now().timestamp()*1000)

    #
    # Write to Table
    table.put_item(
       Item = {
            'controlId': get_uuid,
            'timestamp': tsi
            }
    )
    #
    # Send mqtt message
    client = boto3.client('iot-data', region_name='eu-central-1')

    # Change topic, qos and payload
    ItemToSend = {
        'controlId': get_uuid
        }
    responseMqtt = client.publish(
        topic='poleGroup/group1',
        qos=0,
        payload=json.dumps(ItemToSend)
    )
    print('Send to topic %s',json.dumps(ItemToSend))
    body = {
        "message": "DynamoDb written successfully!",
        "input": get_uuid
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response
