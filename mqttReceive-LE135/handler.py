import json
import boto3
import os
import logging
import datetime


logger = logging.getLogger()
logger.setLevel(logging.INFO)

def mqttReceive(event, context):

    dynamodb = boto3.resource('dynamodb')
    tableReceived = dynamodb.Table(os.environ['REPLY_MESSAGES_TABLE'])
    tableSent = dynamodb.Table(os.environ['CONTROL_MESSAGES_TABLE'])

    logger.info(json.dumps(event))

    # Incoming message format
    #	{
    #     "deviceId": "XX:YY.."
	#	  "controlId": "b9eaf0fa-a4c9-5553-9f7d-b02164979338"
    #   }
    get_deviceid = event["deviceId"]
    get_uuid = event["controlId"]
    print(f'deviceId: {get_deviceid}')
    print(f'controlId: {get_uuid}')

    # get time
    timeReceived =(int)(datetime.datetime.now().timestamp()*1000)

    # Read with controlId the timestamp from GROUP_SEND_TABLE
    dbResponse = tableSent.get_item(
        Key = {
            'controlId': get_uuid
            }
    )

    itemSent = dbResponse['Item']
    timeSent = itemSent['timestamp']
    timeDelta = timeReceived-timeSent
    print(f'timeSent: {timeSent}, timeReceived: {timeReceived}, timeDelta: {timeDelta}')

    # Write deviceId, controlId and timeDelta to REPLY_MESSAGES_TABLE
    tableReceived.put_item(
       Item = {
            'controlId': get_uuid,
            'deviceId': get_deviceid,
            'timedelta': timeDelta,
            'timestamp': timeReceived
            }
    )

    body = {
        "message": "DynamoDb written successfully!",
        "input": get_deviceid
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response
